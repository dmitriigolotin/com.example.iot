package com.example.iot.controller;


import com.example.iot.model.ReadingInput;
import com.example.iot.service.DataInputService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value="Data input controller")
@RestController
@RequestMapping(value = "/reading")
public class DataInputController {

    @Autowired
    private DataInputService dataInputService;

    @ApiOperation(value = "Add a new value to a sensors data")
    @PostMapping
    public void addReading(@RequestBody ReadingInput readingInput) {
        dataInputService.addReading(
                readingInput.getSensorId(),
                readingInput.getSecretToken(),
                readingInput.getTemperature(),
                readingInput.getLatitude(),
                readingInput.getLongitude());
    }


}