package com.example.iot.controller;

import com.example.iot.domain.Reading;
import com.example.iot.service.ReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;
import java.util.Map;

@Api(value="Main controller")
@Controller
public class MainController {

    @Autowired
    private ReadingService readingService;

    @ApiOperation(value = "Greeting view")
    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @ApiOperation(value = "View a list of available data")
    @GetMapping("/main")
    public String main(@RequestParam(required = false, defaultValue = "") String city, Model model) {
        List<Reading> readings;
        if (city != null && !city.isEmpty()) {
            readings = readingService.getSensorLast10ReadingFromCity(city);
        } else {
            readings = readingService.getSensorLast10Reading();
        }
        model.addAttribute("readings", readings);
        model.addAttribute("city", city);
        return "main";
    }

}