package com.example.iot.service;

import com.example.iot.domain.Reading;
import com.example.iot.domain.Sensor;
import com.example.iot.repository.ReadingRepository;
import com.example.iot.repository.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@Component
public class DataInputService {

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private ReadingRepository readingRepository;

    @Autowired
    private GeoService geoService;


    public void addReading(long sensorId, String secretToken, float temperature, double latitude, double longitude) {

        String city;

        if(latitude < -90 || latitude > 90) {
            throw new IllegalArgumentException("Wrong latitude format");
        } else if(longitude < -180 || longitude > 180) {
            throw new IllegalArgumentException("Wrong longitude format");
        } else {
            city = geoService.getCity(latitude, longitude);
        }

        if(!sensorRepository.findById(sensorId).isPresent()) {
            throw new EntityNotFoundException("Sensor is unknown");
        } else {
            Sensor sensor = sensorRepository.findById(sensorId).get();
            if(!sensor.getSecretToken().equals(secretToken)) {
                throw new IllegalArgumentException("Secret token is not correct");
            }

            Reading reading = Reading.builder()
                    .sensor(sensor)
                    .temperature(temperature)
                    .latitude(latitude)
                    .longitude(longitude)
                    .city(city)
                    .build();
            readingRepository.save(reading);
        }
    }

}
