package com.example.iot.service;

import com.example.iot.domain.Reading;

import com.example.iot.repository.ReadingRepository;
import com.example.iot.repository.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
public class ReadingService {

    @Autowired
    ReadingRepository readingRepository;


    public List<Reading> getSensorLast10Reading(long sensorId) {
        return readingRepository.findLast10BySensorId(sensorId);
    }

    public List<Reading> getSensorLast10Reading() {
        return readingRepository.findLast10By();
    }

    public List<Reading> getSensorLast10ReadingFromCity(String city) {
        return readingRepository.findLast10ByCity(city);
    }

}
