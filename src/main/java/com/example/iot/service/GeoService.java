package com.example.iot.service;

import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageResponse;
import com.byteowls.jopencage.model.JOpenCageReverseRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GeoService {

    @Value("${geokey}")
    private String geokey;

    protected String getCity(double latitude, double longitude){
        JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder(geokey);
        JOpenCageReverseRequest request = new JOpenCageReverseRequest(latitude, longitude);
        request.setLanguage("en");
        request.setNoDedupe(true);
        request.setLimit(1);
        request.setNoAnnotations(true);
        request.setMinConfidence(3);

        JOpenCageResponse response = jOpenCageGeocoder.reverse(request);
        String city = "unknown";
        if (response != null && response.getFirstComponents() != null &&
                response.getFirstComponents().getCity() != null){
            city = response.getFirstComponents().getCity();
        }
        return city;
    }
}
