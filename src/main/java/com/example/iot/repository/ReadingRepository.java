package com.example.iot.repository;


import com.example.iot.domain.Reading;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReadingRepository extends JpaRepository<Reading, Long> {
    List<Reading> findLast10BySensorId(Long sensorId);
    List<Reading> findLast10By();
    List<Reading> findLast10ByCity(String city);
}
