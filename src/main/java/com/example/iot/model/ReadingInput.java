package com.example.iot.model;

import lombok.Data;

@Data
public class ReadingInput {
    private long sensorId;
    private String secretToken;
    private float temperature;
    private double latitude;
    private double longitude;
}
