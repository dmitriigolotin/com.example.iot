
<#include "security.ftl">

<table class="table" id="message-list">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Temperature</th>
        <th scope="col">City</th>
        <th scope="col">Timestamp</th>
    </tr>
    </thead>
    <tbody>
    <#list readings as reading>  <tr>
        <th scope="row">${reading.id}</th>
        <td>${reading.temperature}</td>
        <td>${reading.city}</td>
        <td>${reading.timestamp}</td>
    </tr>
    </#list>
    </tbody>
</table>
