INSERT INTO user (id, username, password)
    VALUES ( 1, 'admin', '$2a$08$GXRWbYa/TdihOnsAAunXnub.ybgR1uQs65Pcgc/lO7KEjh8OYGzRK');

INSERT INTO sensor (id, name, secret_token)
    VALUES ( 1, 'sensor1', '12345');

INSERT INTO reading (id, city, latitude, longitude, temperature, timestamp, sensor_id)
    VALUES ( 1, 'Barcelona', '41.40015', '2.15765', '10', '2019-01-01 00:00:00', '1');
INSERT INTO reading (id, city, latitude, longitude, temperature, timestamp, sensor_id)
    VALUES ( 2, 'Cairo', '30.07708', '31.285909', '20', '2019-01-01 00:00:00', '1');

