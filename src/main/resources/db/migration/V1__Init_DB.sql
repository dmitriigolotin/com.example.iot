
create table hibernate_sequence (
        next_val bigint not null
) engine=MEMORY;

INSERT INTO hibernate_sequence (next_val) VALUES (1);

    create table user (
        id bigint not null AUTO_INCREMENT,
        password varchar(255) not null,
        username varchar(255) not null,
        primary key (id)
    ) engine=MEMORY;

    create table reading (
        id bigint not null AUTO_INCREMENT,
        city varchar(255),
        latitude double precision not null,
        longitude double precision not null,
        temperature float not null,
        timestamp DATETIME,
        sensor_id bigint,
        primary key (id)
    ) engine=MEMORY;

    create table sensor (
        id bigint not null AUTO_INCREMENT,
        name varchar(255),
        secret_token varchar(255),
        primary key (id)
    ) engine=MEMORY;

alter table reading add constraint reading_sensor_fk foreign key (sensor_id) references sensor (id);
